//TODO: Add ability to choose department when registering an employee
//TODO: Saving of employee in employees table and of department in departments table should be
//TODO: one  transaction (i.e., atomic)
//TODO: 3. Create a service for DeptService. Create a function that would call and retrieve department information from the server

//TODO 3.1 Create service. You should know how to do this by now so detailed instructions wouldn't be given
// Always use an IIFE, i.e., (function() {})();
(function () {
    // Attaches DeptService service to the DMS module
    angular
        .module("EMS")
        .service("DeptService", DeptService);

    // Dependency injection. Here we inject $http because we need this built-in service to communicate with the server
    // There are different ways to inject dependencies; $inject is minification safe
    DeptService.$inject = ['$http'];

    // DeptService function declaration
    // Accepts the injected dependency as a parameter. We name it $http for consistency, but you may assign any name
    function DeptService($http) {

        // Declares the var service and assigns it the object this (in this case, the DeptService). Any function or
        // variable that you attach to service will be exposed to callers of DeptService, e.g., search.controller.js
        // and register.controller.js
        var service = this;

        // EXPOSED FUNCTIONS -------------------------------------------------------------------------------------------
        // TODO: 3.2 Expose retrieveDepartments function
        service.retrieveDepartments = retrieveDepartments;

        // FUNCTION DECLARATION AND DEFINITION -------------------------------------------------------------------------
        // TODO: 3.1 Create function that would retrieve departments
        function retrieveDepartments() {
            return $http({
                method: 'GET'
                , url: 'api/departments'
            });
        }
    }
})();